import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    problems: "welcome"
}

const calculatorSlice = createSlice({
    name: "calculator",
    initialState,
    reducers: {
        handleProblems: (state, { payload }) => {
            if(state.problems == "welcome"){
                state.problems = ""
            }
            if (payload === "+" || payload === "-" || payload === "*" || payload === "/") {
                const lastChar = state.problems.charAt(state.problems.length - 1)
                if (lastChar === "+" || lastChar === "-" || lastChar === "*" || lastChar === "/") {
                    state.problems = state.problems.substring(0, state.problems.length - 1) + payload
                } else {
                    state.problems = state.problems + payload
                }
            } else {
                state.problems = state.problems + payload
            }
        },
        handleAllClear: (state) => {
            state.problems = "welcome"
        },
        handleBack: (state) => {
            if(state.problems !== "welcome"){
                state.problems = state.problems.substring(0, state.problems.length - 1);
            }
            if(state.problems == ""){
                state.problems = "welcome"
            }
        },
        handleCalculate: (state) => {
            state.problems = eval(state.problems)+""
        }
    }
})

export const { handleProblems, handleAllClear, handleBack, handleCalculate } = calculatorSlice.actions

export default calculatorSlice.reducer