import { configureStore } from '@reduxjs/toolkit'
import calculatorReducer from './reducer'

export const store = configureStore({
    reducer: {
        calculator: calculatorReducer
    }
})