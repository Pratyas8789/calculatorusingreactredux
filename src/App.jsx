import 'bootstrap/dist/css/bootstrap.min.css';
import { } from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux'
import { handleAllClear, handleBack, handleCalculate, handleProblems } from '../redux/reducer';


function App() {

  const { problems } = useSelector(store => store.calculator)
  const dispatch = useDispatch()

  const handleClick = (e) => {
    dispatch(handleProblems(e.target.innerText))
  }

  return (
    <div className='app d-flex justify-content-center align-items-center' >
      <div className="calculator bg-black">
        <div className="display w-100 d-flex justify-content-center align-items-center">
          <div className="output d-flex justify-content-end align-items-center h-75 text-white pe-3 rounded-5 overflow-hidden" >
            <h1>{problems}</h1>
          </div>
        </div>
        <div className="allButtons w-100 d-flex flex-column justify-content-around">
          <div className="buttonRows d-flex justify-content-around p-2">
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-success" onClick={() => dispatch(handleAllClear())} ><h1>AC</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-danger" onClick={() => dispatch(handleBack())}  ><h1>DEL</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-warning" onClick={handleClick} ><h1>/</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-warning" onClick={handleClick} ><h1>*</h1></div>
          </div>
          <div className="buttonRows d-flex justify-content-around p-2" >
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light " onClick={handleClick}>  <h1>7</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light" onClick={handleClick} ><h1>8</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light" onClick={handleClick} ><h1>9</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-warning" onClick={handleClick} ><h1>-</h1></div>
          </div>

          <div className='buttonRows1 d-flex' >
            <div className='w-75 h-100 ' >
              <div className='w-100 h-50  d-flex justify-content-around p-2'>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>4</h1></div>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>5</h1></div>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>6</h1></div>
              </div>
              <div className='w-100 h-50 d-flex justify-content-around p-2'>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>1</h1></div>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>2</h1></div>
                <div className='button w-25 h-100 d-flex justify-content-center align-items-center rounded-circle text-light' onClick={handleClick} ><h1>3</h1></div>
              </div>
            </div>
            <div className='w-25 h-100 p-2 '  >
              <div className=' button w-100 d-flex h-100 justify-content-center align-items-center rounded-circle text-light bg-warning' onClick={handleClick} >
                <h1 >+</h1>
              </div>
            </div>
          </div>
          <div className="buttonRows d-flex justify-content-around p-2">
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light " style={{ width: "40%" }} onClick={handleClick} ><h1>0</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light" onClick={handleClick} ><h1>.</h1></div>
            <div className="button d-flex justify-content-center align-items-center rounded-circle text-light bg-success" onClick={() => dispatch(handleCalculate())}  ><h1>=</h1></div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App